<?php
/*echo '<pre>';
var_dump($_POST); die;*/

try {
	//Conectando à base de dados
    //$db = new PDO('mysql:host=localhost:3310;dbname=copicola;charset=utf8', 'root', '');
    $db = new PDO('mysql:host=localhost;dbname=copicola;charset=utf8', 'copicola', 'c0p1@sp2019');
    
} catch (PDOException $e) {
    print "Erro de conexão com a base: " . $e->getMessage() . "<br/>";
    die();
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$row = array();
$datacadastro = date('Y-m-d H:i:s');

//validação
if(empty($_POST['nome'])||empty($_POST['email'])||$_POST['uf']=='Selecione'||empty($_POST['cidade'])||empty($_POST['organizacao'])
||$_POST['funcao']=='Selecione'||$_POST['setor']=='Selecione'||$_POST['onde_conheceu']=='Selecione'||$_POST['utilizacao']=='Selecione'){
    echo "Por gentileza, preencha todos os campos do formulário :)";
    die;
}

try{
    //Início da transação
    $db->beginTransaction();
    
    //Verificando se já existe o usuário
    $sql = "SELECT Cod_Usr FROM usuarios WHERE Email_Usr = :email";
	$select= $db->prepare($sql);
    $select->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
    $select->execute();
    $row = $select->fetch();
    //print_r($row); die;

    //preparando query de insert de usuario
    if (empty($row['Cod_Usr'])){
        $sql = "INSERT INTO usuarios (Nome_Usr, Email_Usr, RecebNovidades_Usr, Funcao_Usr, Organizacao_Usr, UF_Usr, Cidade_Usr, SetorAtua_Usr, DataCad_Usr) 
                VALUES (:nome, :email, :recebenovidades, :funcao, :organizacao, :uf, :cidade, :setor, :datacad)";
        $stmt= $db->prepare($sql);
        $stmt->bindValue(':nome', $_POST['nome'], PDO::PARAM_STR);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue(':recebenovidades', $_POST['recebenovidades'], PDO::PARAM_STR);
        $stmt->bindValue(':funcao', $_POST['funcao'], PDO::PARAM_STR);
        $stmt->bindValue(':organizacao', $_POST['organizacao'], PDO::PARAM_STR);
        $stmt->bindValue(':uf', $_POST['uf'], PDO::PARAM_STR);
        $stmt->bindValue(':cidade', $_POST['cidade'], PDO::PARAM_STR);
        $stmt->bindValue(':setor', $_POST['setor'], PDO::PARAM_STR);
        $stmt->bindValue(':datacad', $datacadastro, PDO::PARAM_STR);
        $stmt->execute();

        $row['Cod_Usr'] = $db->lastInsertId();
    }else{
        $sql = "UPDATE usuarios 
                SET Nome_Usr = :nome, Email_Usr = :email, RecebNovidades_Usr = :recebenovidades, Funcao_Usr = :funcao, 
                    Organizacao_Usr = :organizacao, UF_Usr = :uf, Cidade_Usr = :cidade, SetorAtua_Usr = :setor
                WHERE Cod_Usr = ".$row['Cod_Usr'];
        $stmt= $db->prepare($sql);
        $stmt->bindValue(':nome', $_POST['nome'], PDO::PARAM_STR);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue(':recebenovidades', $_POST['recebenovidades'], PDO::PARAM_STR);
        $stmt->bindValue(':funcao', $_POST['funcao'], PDO::PARAM_STR);
        $stmt->bindValue(':organizacao', $_POST['organizacao'], PDO::PARAM_STR);
        $stmt->bindValue(':uf', $_POST['uf'], PDO::PARAM_STR);
        $stmt->bindValue(':cidade', $_POST['cidade'], PDO::PARAM_STR);
        $stmt->bindValue(':setor', $_POST['setor'], PDO::PARAM_STR);
        $stmt->execute();
    }
    
    //preparando query de insert de questionário
    $sql = "INSERT INTO questionario (CodUsr_Que, FicouSabendo_Que, Utilizacao_Que, Origem_Que, DataCad_Que) 
            VALUES (:id, :onde_conheceu, :utilizacao, :origem, :datacad)";
	$stmt= $db->prepare($sql);
    $stmt->bindValue(':id', $row['Cod_Usr'], PDO::PARAM_INT);
    $stmt->bindValue(':onde_conheceu', $_POST['onde_conheceu'], PDO::PARAM_STR);
    $stmt->bindValue(':utilizacao', $_POST['utilizacao'], PDO::PARAM_STR);
    $stmt->bindValue(':origem', $_POST['origem'], PDO::PARAM_STR);
    $stmt->bindValue(':datacad', $datacadastro, PDO::PARAM_STR);
    $stmt->execute();

    //Efetuando as operações acima na base de dados
	$db->commit();
	
	//fechando conexão com a base
    $db = null;

    echo "Muito obrigada. Clique nos links abaixo para acessar os materiais:<br>";
    switch($_POST['origem']){
        case 'modal1': 
            echo "<a href='docs/2_zonaazul_onepage.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Onepage do caso Zona Azul</a><br>";
            echo "<a href='docs/2_guia_zonaazul.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Guia do caso Zona Azul Digital </a>";
            break;
        case 'modal2': 
            echo "<a href='docs/1_empreenda_onepage.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Onepage do caso Empreenda Fácil</a><br>";
            echo "<a href='docs/1_guia_empreenda_facil.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Guia do caso Empreenda Fácil</a>";
            break;
        case 'modal3': 
            echo "<a href='docs/5_bdgsmul_onepage.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Onepage do caso Banco de Dados Geográficos da SMUL</a><br>";
            echo "<a href='docs/5_guia_bdgsmul.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Guia do caso Banco de Dados Geográficos da SMUL</a>";
            break;
        case 'modal4': 
            echo "<a href='docs/4_pratoaberto_onepage.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Onepage do caso Prato Aberto</a><br>";
            echo "<a href='docs/4_guia_pratoaberto.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Guia do caso Prato Aberto</a>";
            break;
        case 'modal5': 
            echo "<a href='docs/0_concurso_de_projetos_onepage.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Onepage do caso Concurso de Projetos do Mobilab</a><br>";
            echo "<a href='docs/0_concurso_de_projetos.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Guia do caso Concurso de Projetos do Mobilab</a>";
            break;
        case 'modal6': 
            echo "<a href='docs/copicola.pdf' target='_blank'><img src='img/pdf_icon.png' style='margin-bottom: 0px;'/> Copicola</a>";
            break;
    }

} catch (Exception $e) {
    $db->rollBack();
    echo "Falha ao salvar dados: " . $e->getMessage();
}
?>