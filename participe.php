<?php
/*echo '<pre>';
var_dump($_POST); die;*/

try {
	//Conectando à base de dados
    //$db = new PDO('mysql:host=localhost:3310;dbname=copicola;charset=utf8', 'root', '');
    $db = new PDO('mysql:host=localhost;dbname=copicola;charset=utf8', 'copicola', 'c0p1@sp2019');
    
} catch (PDOException $e) {
    print "Erro de conexão com a base: " . $e->getMessage() . "<br/>";
    die();
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$row = array();
$datacadastro = date('Y-m-d H:i:s');

//validação
if(empty($_POST['nome'])||empty($_POST['email'])||empty($_POST['descricao_iniciativa'])||empty($_POST['responsaveis_iniciativa'])||$_POST['organizacao']=='Selecione'||$_POST['funcao']=='Selecione'){
    echo "Favor preencher os campos do formulário.";
    die;
}

try{
    //Início da transação
    $db->beginTransaction();
    
    //Verificando se já existe o usuário
    $sql = "SELECT Cod_Usr FROM usuarios WHERE Email_Usr = :email";
	$select= $db->prepare($sql);
    $select->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
    $select->execute();
    $row = $select->fetch();
    //print_r($row); die;

    //preparando query de insert de usuario
    if (empty($row['Cod_Usr'])){
        $sql = "INSERT INTO usuarios (Nome_Usr, Email_Usr, Funcao_Usr, Organizacao_Usr, DataCad_Usr) 
                VALUES (:nome, :email, :funcao, :organizacao, :datacad)";
        $stmt= $db->prepare($sql);
        $stmt->bindValue(':nome', $_POST['nome'], PDO::PARAM_STR);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue(':funcao', $_POST['funcao'], PDO::PARAM_STR);
        $stmt->bindValue(':organizacao', $_POST['organizacao'], PDO::PARAM_STR);
        $stmt->bindValue(':datacad', $datacadastro, PDO::PARAM_STR);
        $stmt->execute();

        $row['Cod_Usr'] = $db->lastInsertId();
    }else{
        $sql = "UPDATE usuarios 
                SET Nome_Usr = :nome, Email_Usr = :email, Funcao_Usr = :funcao, Organizacao_Usr = :organizacao
                WHERE Cod_Usr = ".$row['Cod_Usr'];
        $stmt= $db->prepare($sql);
        $stmt->bindValue(':nome', $_POST['nome'], PDO::PARAM_STR);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue(':funcao', $_POST['funcao'], PDO::PARAM_STR);
        $stmt->bindValue(':organizacao', $_POST['organizacao'], PDO::PARAM_STR);
        $stmt->execute();
    }
    
    //preparando query de insert de questionário
    $sql = "INSERT INTO questionario (CodUsr_Que, Sistematizar_Que, QuemFez_Que, FicouSabendo_Que, Origem_Que, DataCad_Que) 
            VALUES (:id, :descricao_iniciativa, :responsaveis_iniciativa, :ficou_sabendo, :origem, :datacad)";
	$stmt= $db->prepare($sql);
    $stmt->bindValue(':id', $row['Cod_Usr'], PDO::PARAM_INT);
    $stmt->bindValue(':descricao_iniciativa', $_POST['descricao_iniciativa'], PDO::PARAM_STR);
    $stmt->bindValue(':responsaveis_iniciativa', $_POST['responsaveis_iniciativa'], PDO::PARAM_STR);
    $stmt->bindValue(':ficou_sabendo', $_POST['ficou_sabendo'], PDO::PARAM_STR);
    $stmt->bindValue(':origem', $_POST['origem'], PDO::PARAM_STR);
    $stmt->bindValue(':datacad', $datacadastro, PDO::PARAM_STR);
    $stmt->execute();

    //Efetuando as operações acima na base de dados
	$db->commit();
	
	//fechando conexão com a base
    $db = null;

    echo "Dados salvos com sucesso. Obrigado pela sua contribuição!";
} catch (Exception $e) {
    $db->rollBack();
    echo "Falha ao salvar dados: " . $e->getMessage();
}
?>